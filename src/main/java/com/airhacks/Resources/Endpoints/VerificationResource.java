package com.airhacks.Resources.Endpoints;

import com.airhacks.JPA.UserT;
import com.airhacks.JPA.VerificationTokenT;
import com.airhacks.Persistence.Service.TokenService;
import com.airhacks.Persistence.Service.UserService;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author airhacks.com
 */
@Path("verifyToken")
public class VerificationResource implements Serializable {

    @Inject
    private UserService userService;

    @Inject
    private TokenService tokenService;

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse response;



    //localhost:8080/com.ultimateTodo/resources/verifyToken/hashToken
    @GET
    @Path("{tokenHash}")
    @Produces("text/html")
    public Response verifyUserByToken(@PathParam("tokenHash") String tokenHash){
        String contextPath = request.getContextPath();
        VerificationTokenT verificationToken = tokenService.findTokenByHashToken(tokenHash);
        if(verificationToken==null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime()));
        if(verificationToken.getExpirationDate().before(new Date(cal.getTime().getTime()))) {
            return Response.status(Response.Status.GONE).build();
        }

        UserT user = verificationToken.getUser();
            user.setEnabled(true);
            userService.saveUser(user);
        String myJsfPage = "/login.xhtml";
        try {
            response.sendRedirect(contextPath + myJsfPage);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Response.status(Response.Status.ACCEPTED).build();
    }

}

package com.airhacks.Resources;

import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Application;
import java.io.Serializable;

/**
 * Configures a JAX-RS endpoint. Delete this class, if you are not exposing
 * JAX-RS resources in your application.
 *
 * @author airhacks.com
 */
@ApplicationPath("resources")
public class ApplicationResource extends Application implements Serializable {

    private static Client applicationClient;

    @PostConstruct
    private void init(){
        applicationClient = ClientBuilder.newClient();
    }

    public static Client getApplicationClient() {
        return applicationClient;
    }

    public static void setApplicationClient(Client applicationClient) {
        ApplicationResource.applicationClient = applicationClient;
    }
}

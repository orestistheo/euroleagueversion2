package com.airhacks.Bean;

import com.airhacks.Domain.FantasyLeagueD;
import com.airhacks.Domain.enums.FantasyLeagueTypeEnum;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named("fantasyLeagueBean")
@ViewScoped
public class FantasyLeagueBean implements Serializable {

    static final long serialVersionUID = 1L;


    @Inject
    private transient Logger logger;

    private FantasyLeagueD fantasyLeagueD;
    private List<SelectItem> fantasyLeagueTypes;

    @PostConstruct
    private void init(){
        logger.info("Initializing fantasy league bean");
        fantasyLeagueD = new FantasyLeagueD();
        fantasyLeagueTypes= FantasyLeagueTypeEnum.getEnumValues();
    }

    public FantasyLeagueD getFantasyLeagueD() {
        return fantasyLeagueD;
    }

    public void setFantasyLeagueD(FantasyLeagueD fantasyLeagueD) {
        this.fantasyLeagueD = fantasyLeagueD;
    }

    public List<SelectItem> getFantasyLeagueTypes() {
        return fantasyLeagueTypes;
    }

    public void setFantasyLeagueTypes(List<SelectItem> fantasyLeagueTypes) {
        this.fantasyLeagueTypes = fantasyLeagueTypes;
    }
}

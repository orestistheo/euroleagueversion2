package com.airhacks.Bean;

import com.airhacks.Domain.UserD;
import com.airhacks.Domain.enums.GenderEnum;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Named("registerBean")
@ViewScoped
public class RegisterBean implements Serializable {

    @Inject
    private transient Logger logger;

    private UserD user;
    private String repeatPaasword;
    private List<SelectItem> genderEnums;



    @PostConstruct
    private void init(){
        logger.info("Initializing event bean");
        setUser(new UserD());
        getUser().getUserInfo().setDateOfBirth(LocalDate.now().minusYears(20L));
        genderEnums= GenderEnum.getEnumValues();
    }


    public UserD getUser() {
        return user;
    }

    public void setUser(UserD user) {
        this.user = user;
    }

    public String getRepeatPaasword() {
        return repeatPaasword;
    }

    public void setRepeatPaasword(String repeatPaasword) {
        this.repeatPaasword = repeatPaasword;
    }

    public List<SelectItem> getGenderEnums() {
        return genderEnums;
    }

    public void setGenderEnums(List<SelectItem> genderEnums) {
        this.genderEnums = genderEnums;
    }
}

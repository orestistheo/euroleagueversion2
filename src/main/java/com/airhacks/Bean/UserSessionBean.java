package com.airhacks.Bean;

import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@SessionScoped
@Named("userSessionBean")
public class UserSessionBean implements Serializable {

    private String username;
    private String password;
    private Long userId;
    private String langCode;
    private Boolean loggedIn;
    private final String DATE_PATTERN = "dd/MM/yyyy";
    private Boolean editMode;



    @PostConstruct
    private void init(){
        setLoggedIn(false);
    }

    public void clear(){
        this.username = null;
        this.password = null;
        this.userId = null;
        this.langCode = null;
        this.loggedIn = false;
        this.editMode = false;
    }


    public UserSessionBean() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public Boolean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getDATE_PATTERN() {
        return DATE_PATTERN;
    }

    public Boolean getEditMode() {
        return editMode;
    }

    public void setEditMode(Boolean editMode) {
        this.editMode = editMode;
    }

    @Override
    public String toString() {
        return "UserSessionBean{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", userId=" + userId +
                ", langCode='" + langCode + '\'' +
                ", editMode='" + editMode + '\'' +
                ", LoggedIn=" + loggedIn +
                '}';
    }
}

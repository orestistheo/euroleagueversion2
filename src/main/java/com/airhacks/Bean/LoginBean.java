package com.airhacks.Bean;

import com.airhacks.Domain.UserD;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named("loginBean")
@ViewScoped
public class LoginBean implements Serializable {

    @Inject
    private transient Logger logger;

    private UserD user;

    @PostConstruct
    private void init(){
        logger.info("Initializing login bean");
        setUser(new UserD());
    }

    public UserD getUser() {
        return user;
    }

    public void setUser(UserD user) {
        this.user = user;
    }


}

package com.airhacks.Bean;

import com.airhacks.Domain.FantasyTeamD;
import com.airhacks.Domain.UserD;
import org.apache.log4j.Logger;
import org.primefaces.model.UploadedFile;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named("profileBean")
@ViewScoped
public class ProfileBean implements Serializable {
    @Inject
    private transient Logger logger;

    private UserD user;

    private List<FantasyTeamD> userTeams;

    private UploadedFile uploadedFile;




    public UserD getUser() {
        return user;
    }

    public void setUser(UserD user) {
        this.user = user;
    }

    public List<FantasyTeamD> getUserTeams() {
        return userTeams;
    }

    public void setUserTeams(List<FantasyTeamD> userTeams) {
        this.userTeams = userTeams;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }
}

package com.airhacks.Controller;

import com.airhacks.Bean.FantasyLeagueBean;
import com.airhacks.Bean.UserSessionBean;
import com.airhacks.Persistence.ClientService.ProfileClientService;
import org.apache.log4j.Logger;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named("fantasyLeagueContr")
@ViewScoped
public class FantasyLeagueController implements Serializable {

    static final long serialVersionUID = 1L;

    @Inject
    private transient Logger logger;

    @Inject
    private FantasyLeagueBean fantasyLeagueBean;

    @Inject
    private ProfileClientService profileClientService;

    @Inject
    private UserSessionBean userSessionBean;



    public FantasyLeagueBean getFantasyLeagueBean() {
        return fantasyLeagueBean;
    }

    public void setFantasyLeagueBean(FantasyLeagueBean fantasyLeagueBean) {
        this.fantasyLeagueBean = fantasyLeagueBean;
    }

    public void createFantasyLeague()  {
        logger.info("Creating league");
        profileClientService.createFantasyLeague(fantasyLeagueBean.getFantasyLeagueD(),userSessionBean.getUserId());
    }
}

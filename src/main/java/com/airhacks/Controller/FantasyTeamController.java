package com.airhacks.Controller;

import com.airhacks.Bean.FantasyTeamBean;
import org.apache.log4j.Logger;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;


@Named("fantasyTeamContr")
@ViewScoped
public class FantasyTeamController implements Serializable {

    static final long serialVersionUID = 1L;

    @Inject
    private transient Logger logger;

    @Inject
    private FantasyTeamBean fantasyTeamBean;

    public FantasyTeamBean getFantasyTeamBean() {
        return fantasyTeamBean;
    }

    public void setFantasyTeamBean(FantasyTeamBean fantasyTeamBean) {
        this.fantasyTeamBean = fantasyTeamBean;
    }
}

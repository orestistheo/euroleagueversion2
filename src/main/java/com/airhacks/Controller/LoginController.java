package com.airhacks.Controller;

import com.airhacks.Bean.RegisterBean;
import com.airhacks.Bean.LoginBean;
import com.airhacks.Persistence.Service.LoginService;
import com.airhacks.Persistence.Service.UserService;
import org.apache.log4j.Logger;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;


@Named("loginController")
@ViewScoped
public class LoginController implements Serializable {

    @Inject
    private RegisterBean registerBean;

    @Inject
    private LoginBean loginBean;

    @Inject
    private transient Logger logger;

    @Inject
    private UserService userService;

    @Inject
    private LoginService loginService;




    public String login(){
        logger.info("Attempting Log In");
        return loginService.login(loginBean.getUser().getUserName(),loginBean.getUser().getPassword());
    }

    public String logout(){
        logger.info("Attempting Log Out");
        return loginService.logout();
    }

    public void createNewUser(){
        logger.info("Creating New User");
        userService.createNewUser(registerBean.getUser(), registerBean.getRepeatPaasword());
    }



    public RegisterBean getRegisterBean() {
        return registerBean;
    }

    public void setRegisterBean(RegisterBean registerBean) {
        this.registerBean = registerBean;
    }



}

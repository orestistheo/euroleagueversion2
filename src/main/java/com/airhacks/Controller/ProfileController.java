package com.airhacks.Controller;

import com.airhacks.Bean.ProfileBean;
import com.airhacks.Bean.UserSessionBean;
import com.airhacks.Domain.enums.GenderEnum;
import com.airhacks.Persistence.ClientService.ProfileClientService;
import com.airhacks.Persistence.Service.ProfileService;
import com.airhacks.Persistence.Service.UserService;
import com.airhacks.Persistence.Mappers.UserMapper;
import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named("profileController")
@ViewScoped
public class ProfileController implements Serializable {

    @Inject
    private transient Logger logger;

    @Inject
    private UserService userService;

    @Inject
    private ProfileService profileService;

    @Inject
    private ProfileBean profileBean;

    @Inject
    private UserSessionBean userSessionBean;

    @Inject
    private ProfileClientService profileClientService;

    private UploadedFile file;


    @PostConstruct
    private void init() {
        logger.info("Initializing profile bean");
        profileBean.setUser(UserMapper.convertUserTtoD(userService.findUserById(userSessionBean.getUserId())));

            profileBean.setUserTeams(profileClientService.findFantasyTeamsByUser(userSessionBean.getUserId()));
    }

    public void upload() {
        if (file != null) {
            FacesMessage message = new FacesMessage("Successful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public void handleFileUpload(FileUploadEvent event) {
        UploadedFile uploadedFile = event.getFile();
        byte[] content = uploadedFile.getContents();

        profileService.uploadNewImageForUser(content, userSessionBean.getUserId());
        FacesMessage msg = new FacesMessage("Successful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public String findGenderDescription() {
        try {
            return GenderEnum.getEnumById(profileBean.getUser().getUserInfo().getGender().getId()).getDescription();
        } catch (NullPointerException e) {
            logger.error(e.getMessage());
            return null;
        }
    }


    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public ProfileBean getProfileBean() {
        return profileBean;
    }

    public void setProfileBean(ProfileBean profileBean) {
        this.profileBean = profileBean;
    }
}

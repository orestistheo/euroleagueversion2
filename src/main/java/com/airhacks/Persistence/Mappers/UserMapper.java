package com.airhacks.Persistence.Mappers;

import com.airhacks.JPA.UserInfoT;
import com.airhacks.JPA.UserT;
import com.airhacks.Domain.UserD;
import com.airhacks.Domain.UserInfoD;
import com.airhacks.Domain.enums.GenderEnum;

public class UserMapper {

    public static UserD convertUserTtoD(UserT userT){
        UserD userD = new UserD();
        userD.setEmail(userT.getEmail());
        userD.setPassword(userT.getPassword());
        userD.setUserId(userT.getUserId());
        userD.setUserName(userT.getUserName());
        userD.addUserInfo(UserMapper.convertUserInfoTtoD(userT.getUserInfo()));

        return userD;
    }

    public static UserT convertUserDtoT(UserD userD){
        UserT userT = new UserT();
        userT.setEmail(userD.getEmail());
        userT.setPassword(userD.getPassword());
        userT.setUserId(userD.getUserId());
        userT.setUserName(userD.getUserName());
        userT.addUserInfo(UserMapper.convertUserInfoDtoT(userD.getUserInfo()));
        return userT;
    }

    public static UserInfoD convertUserInfoTtoD(UserInfoT userInfoT){
        UserInfoD userInfoD = new UserInfoD();
        userInfoD.setDateOfBirth(userInfoT.getDateOfBirth());
        userInfoD.setUserInfoId(userInfoT.getUserInfoId());
//        userInfoD.setFavouriteTeam(userInfoT.getFavouriteTeam());
         userInfoD.setUserIcon(userInfoT.getUserIcon());
        if(userInfoT.getGender()!=null)
            userInfoD.setGenderString(userInfoT.getGender().getId());
        return userInfoD;
    }

    public static UserInfoT convertUserInfoDtoT(UserInfoD userInfoD){
        UserInfoT userInfoT = new UserInfoT();
        userInfoT.setDateOfBirth(userInfoD.getDateOfBirth());
        userInfoT.setUserInfoId(userInfoD.getUserInfoId());
        userInfoT.setUserIcon(userInfoD.getUserIcon());
        if(userInfoD.getGenderString()!=null && !userInfoD.getGenderString().equals(""))
            userInfoT.setGender(GenderEnum.getEnumById(userInfoD.getGenderString()));

        return userInfoT;
    }

}

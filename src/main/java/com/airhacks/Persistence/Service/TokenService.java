package com.airhacks.Persistence.Service;

import com.airhacks.JPA.VerificationTokenT;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

@Stateless
public class TokenService {

    @Inject
    private EntityManager entityManager;

    @Inject
    private Logger logger;


    public VerificationTokenT persistToken(VerificationTokenT verificationToken) {
        logger.info("Persisting token");
        entityManager.persist(verificationToken);
        return verificationToken;
    }

    public VerificationTokenT updateToken(VerificationTokenT token) {
        logger.info("Updating token");
        entityManager.merge(token);
        return token;
    }

    public VerificationTokenT findTokenById(Long id) {
        logger.info("Find token by id");
        return entityManager.find(VerificationTokenT.class, id);
    }

    public List<VerificationTokenT> findAllTokens() {
        logger.info("Find all tokens");
        return entityManager.createQuery("select token from VerificationTokenT token", VerificationTokenT.class)
                .getResultList();
    }

    public VerificationTokenT findTokenByHashToken(String hashToken) {
        logger.info("Find token by hash token");
        try {
            return entityManager.createQuery("select verificationToken from VerificationTokenT verificationToken " +
                    " where verificationToken.token = :hashToken", VerificationTokenT.class)
                    .setParameter("hashToken", hashToken)
                    .getSingleResult();
        }
        catch (Exception e){
            return null;
        }
    }
}

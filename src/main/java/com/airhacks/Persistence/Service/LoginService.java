package com.airhacks.Persistence.Service;

import com.airhacks.Bean.UserSessionBean;
import com.airhacks.JPA.UserT;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.security.Principal;

import static org.apache.log4j.helpers.UtilLoggingLevel.SEVERE;

@Stateless
@Transactional
public class LoginService implements Serializable {

    @Inject
    private UserService userService;

    @Inject
    private transient Logger logger;

    @Inject
    private UserSessionBean userSessionBean;


    public String login(String username, String password) {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        if (!userSessionBean.getLoggedIn()) {
            UserT userT = userService.findUserByUsernameAndEnabled(username, false);
            if (userT == null) {
                try {
                    request.login(username, password);
                    logger.info("Authentication done for user: " + username);
                } catch (Exception e) {
                    logger.info("Login failed");
//                    FacesMessage msg = new FacesMessage("", "Credentials do not match");
//                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
//                    UIComponent child = context.getViewRoot().findComponent("j_idt7:bodyForm:loginUserName");
//                    String loginMsg = child.getClientId();
//                    context.addMessage(loginMsg, msg);
                    return "false";
                }
                Principal principal = request.getUserPrincipal();
                UserT loggedInUser = userService.findUserByUsername(principal.getName());

                //Set user details in user session bean
                userSessionBean.setPassword(loggedInUser.getPassword());
                userSessionBean.setUserId(loggedInUser.getUserId());
                userSessionBean.setUsername(loggedInUser.getUserName());
                userSessionBean.setLoggedIn(true);
                return "true";

            }

            logger.info("Login failed");
//            FacesMessage msg = new FacesMessage("", "Please enable your profile from your email");
//            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
//            UIComponent child = context.getViewRoot().findComponent("j_idt7:bodyForm:loginUserName");
//            String loginMsg = child.getClientId();
//            context.addMessage(loginMsg, msg);
            return "false";
        }
        return "true";
    }

    public String logout() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            userSessionBean.clear();
            request.logout();
            // clear the session
            ((HttpSession) context.getExternalContext().getSession(false)).invalidate();
        } catch (ServletException e) {
            logger.log(SEVERE, "Failed to logout user!", e);
            return "/login.xhtml?faces-redirect = true";
        }
        return "/login.xhtml?faces-redirect = true";
    }

}


package com.airhacks.Persistence.Service;

import com.airhacks.JPA.*;
import com.airhacks.Domain.UserD;
import com.airhacks.Persistence.ClientService.ProfileClientService;
import com.airhacks.Persistence.Mappers.UserMapper;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Stateless
public class UserService implements Serializable {

    @Inject
    private EntityManager entityManager;

    @Inject
    private Logger logger;

    @Inject
    private HashingService hashingService;

    @Inject
    private TokenService tokenService;

    @Inject
    private MailService mailService;

    @Inject
    private ProfileClientService profileClientService;


    public UserT persistUser(UserT userT) {
        logger.info("Persisting user");
        entityManager.persist(userT);
        return userT;
    }

    public UserT saveUser(UserT userT) {
        if (userT.getUserId() == null) {
            entityManager.persist(userT);
        } else {
            entityManager.merge(userT);
        }
        return userT;
    }

    public UserT updateUser(UserT userT) {
        logger.info("Updating user");
        entityManager.merge(userT);
        return userT;
    }

    public UserT findUserById(Long id) {
        logger.info("Find User by id");
        return entityManager.find(UserT.class, id);
    }

    public List<UserT> findAllUsers() {
        logger.info("Find all users");
        return entityManager.createQuery("select user from UserT user", UserT.class)
                .getResultList();
    }

    public List<String> findAllUsernames() {
        logger.info("Find all usernames");
        return entityManager.createQuery("select user.userName from UserT user", String.class)
                .getResultList();
    }

    public List<String> findAllEmails() {
        logger.info("Find all emails");
        return entityManager.createQuery("select user.email from UserT user", String.class)
                .getResultList();
    }

    public UserT findUserByUsername(String username) {
        try {
            return entityManager.createQuery("select user from UserT " +
                    " user where user.userName = :username", UserT.class)
                    .setParameter("username", username)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public UserT findUserByUsernameAndEnabled(String username, Boolean enabled) {
        try {
            return entityManager.createQuery("select user from UserT " +
                    " user where user.userName = :username and user.enabled= :enabled", UserT.class)
                    .setParameter("username", username)
                    .setParameter("enabled", enabled)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }


    public boolean validateNewUser(String username, String password, String repeatPassword, String email) {
        boolean flag = true;
        FacesContext facesContext = FacesContext.getCurrentInstance();
        List<String> emails;
        List<String> usernames;

        if (email == null || email.equals("")) {
            logger.info("Empty email field");
            flag = false;
            FacesMessage msg = new FacesMessage("", "Empty email field");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            UIComponent child = facesContext.getViewRoot().findComponent("j_idt5:registerForm:email");
            String emailId = child.getClientId();
            facesContext.addMessage(emailId, msg);
        } else {
            emails = findAllEmails();
            if (emails != null && emails.contains(email)) {
                logger.info("Email already in use");
                flag = false;
                FacesMessage msg = new FacesMessage("", "Email already in use");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                UIComponent child = facesContext.getViewRoot().findComponent("j_idt5:registerForm:email");
                String emailId = child.getClientId();
                facesContext.addMessage(emailId, msg);
            }
        }

        if (username == null || username.equals("")) {
            logger.info("Empty username field");
            flag = false;
            FacesMessage msg = new FacesMessage("", "Empty username field");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            UIComponent child = facesContext.getViewRoot().findComponent("j_idt5:registerForm:userName");
            String userNameId = child.getClientId();
            facesContext.addMessage(userNameId, msg);
        } else {
            usernames = findAllUsernames();
            if (usernames != null && usernames.contains(username)) {
                logger.info("Username already in use");
                flag = false;
                FacesMessage msg = new FacesMessage("", "Username already in use");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                UIComponent child = facesContext.getViewRoot().findComponent("j_idt5:registerForm:userName");
                String userNameId = child.getClientId();
                facesContext.addMessage(userNameId, msg);
            }

        }

        if (password == null || password.equals("")) {
            logger.info("Empty password field");
            flag = false;
            FacesMessage msg = new FacesMessage("Empty password field");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            UIComponent child = facesContext.getViewRoot().findComponent("j_idt5:registerForm:password");
            String passwordId = child.getClientId();
            facesContext.addMessage(passwordId, msg);
            facesContext.renderResponse();
        } else {
            String response = validatePassword(password);
            if (!response.equals("Correct")) {
                logger.info(response);
                flag = false;
                FacesMessage msg = new FacesMessage(response);
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                UIComponent child = facesContext.getViewRoot().findComponent("j_idt5:registerForm:password");
                String passwordId = child.getClientId();
                facesContext.addMessage(passwordId, msg);
            }
        }

        if (repeatPassword == null || repeatPassword.equals("")) {
            logger.info("Empty repeat password field");
            flag = false;
            FacesMessage msg = new FacesMessage("Empty repeat password field");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            UIComponent child = facesContext.getViewRoot().findComponent("j_idt5:registerForm:repeatPassword");
            String repeatPasswordId = child.getClientId();
            facesContext.addMessage(repeatPasswordId, msg);
        } else if (!repeatPassword.equals(password)) {
            logger.info("Password does not match repeat password");
            flag = false;
            FacesMessage msg = new FacesMessage("Password does not match repeat password");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            UIComponent child = facesContext.getViewRoot().findComponent("j_idt5:registerForm:repeatPassword");
            String repeatPasswordId = child.getClientId();
            facesContext.addMessage(repeatPasswordId, msg);
        }

        return flag;
    }

    //simple length validation
    public String validatePassword(String password) {
        String pattern;
        if (password.length() < 8) {
            return "Password must be at least 8 digits";
        } else if (password.matches("(?=.*[0-9])")) {
            return "Password must contain at least one digit";
        } else if (password.matches("(?=.*[a-z]) ")) {
            return "Password must contain at least one letter";
        }

        return "Correct";
    }

    public String createVerificationTokenForUser(UserT userT) {
        VerificationTokenT verificationToken = new VerificationTokenT();
        verificationToken.setUser(userT);
        verificationToken.setToken(UUID.randomUUID().toString());
        tokenService.persistToken(verificationToken);
        return verificationToken.getToken();
    }

    public void createNewUser(UserD user, String repeatPassword) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        try {

            if (validateNewUser(user.getUserName(), user.getPassword(), repeatPassword, user.getEmail())) {
                logger.info("Succesfull Validation");
                String hashedPass = hashingService.encodeSHA256(user.getPassword());
                user.setPassword(hashedPass);
                UserT userT = UserMapper.convertUserDtoT(user);
                userT = assignRoleToUser("user", userT);
                persistUser(userT);
                //Send verification email to user
                String hashToken = createVerificationTokenForUser(userT);
                StringBuilder stringBuilder = new StringBuilder("<html> Welcome to Euroleague Fantasy. Please click the link to verify your email: <a href='http://localhost:8080/com.ultimateTodo/resources/verifyToken/").append(hashToken).append("'>Click the link to verify your account</a></html>");
                mailService.sendEmail(stringBuilder.toString(), user.getEmail());
                //Create fantasy user
                profileClientService.createFantasyUser(userT.getUserId());

                FacesMessage msg = new FacesMessage("Successful registration", "We have sent you a verification email. Please verify your email to login.");
                msg.setSeverity(FacesMessage.SEVERITY_INFO);
                facesContext.addMessage(null, msg);
            }

        } catch (Exception e) {
            FacesMessage msg = new FacesMessage("Registration failed", e.getMessage());
            msg.setSeverity(FacesMessage.SEVERITY_INFO);
            logger.info(e.getMessage());
        }
    }

    public UserT assignRoleToUser(String roleName, UserT userT) {
        UserRoleT userRoleT = new UserRoleT();
        RoleT role = findRoleByRoleName(roleName);
        userRoleT.setUser(userT);
        userRoleT.setRole(role);
        userT.addUserRole(userRoleT);
        return userT;
    }


    public RoleT findRoleByRoleName(String roleName) {
        try {
            return entityManager.createQuery("select role from RoleT " +
                    " role where role.roleName = :roleName", RoleT.class)
                    .setParameter("roleName", roleName)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

}

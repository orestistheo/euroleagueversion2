package com.airhacks.Persistence.Service;


import com.sun.mail.smtp.SMTPTransport;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

@Named("mailService")
@Stateless
public class MailService {
    private static final String SMTP_SERVER = "smtp.googlemail.com";
    private static final String USERNAME = "otheormail@gmail.com";
    private static final String PASSWORD = "0000Ashe";

    private static final String EMAIL_FROM = "otheormail@gmail.com";

    private static final String EMAIL_SUBJECT = "Test Send Email via SMTP";

    public void sendEmail(String content,String emailTo) {


        //for properties file
//        try (InputStream input = new FileInputStream("path/to/config.properties")) {
//
//            Properties prop = new Properties();
//
//            // load a properties file
//            prop.load(input);
//
//            // get the property value and print it out
//            System.out.println(prop.getProperty("db.url"));
//            System.out.println(prop.getProperty("db.user"));
//            System.out.println(prop.getProperty("db.password"));
//
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
        Properties prop = System.getProperties();
        prop.put("mail.smtp.host", SMTP_SERVER); //optional, defined in SMTPTransport
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.port", "587"); // default port 25
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.ssl.trust", "smtp.googlemail.com");


        Session session = Session.getInstance(prop, null);
        Message msg = new MimeMessage(session);


        try {

            // from
            msg.setFrom(new InternetAddress(EMAIL_FROM));

            // to
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(emailTo, false));


            // subject
            msg.setSubject(EMAIL_SUBJECT);

            // content
            msg.setContent(content,"text/html");

            msg.setSentDate(new Date());

            // Get SMTPTransport
            SMTPTransport t = (SMTPTransport) session.getTransport("smtp");

            // connect
            t.connect(SMTP_SERVER, USERNAME, PASSWORD);

            t.setStartTLS(true);

            // send
            t.sendMessage(msg, msg.getAllRecipients());

            System.out.println("Response: " + t.getLastServerResponse());

            t.close();

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public boolean validateAddress(String emailAddress){
        try {
            InternetAddress address = new InternetAddress(emailAddress);
            address.validate();
            return true;
        } catch (AddressException e) {
            e.printStackTrace();
            return false;
        }
    }
}
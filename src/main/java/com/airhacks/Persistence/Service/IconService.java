package com.airhacks.Persistence.Service;

import com.airhacks.JPA.UserT;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;

@Named("iconService")
@ApplicationScoped
public class IconService implements Serializable {


    @Inject
    private UserService userService;


    public StreamedContent getUserImage() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        }
        else {
            // So, browser is requesting the image. Return a real StreamedContent with the image bytes.
            String studentId = context.getExternalContext().getRequestParameterMap().get("studentId");
            UserT userT = userService.findUserById(Long.valueOf(studentId));
            return new DefaultStreamedContent(new ByteArrayInputStream(userT.getUserInfo().getUserIcon()));
        }
    }

}

package com.airhacks.Persistence.Service;

import com.airhacks.Bean.UserSessionBean;
import com.airhacks.JPA.UserT;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.io.Serializable;

@Stateless
@Transactional
public class ProfileService implements Serializable {

    @Inject
    private transient Logger logger;

    @Inject
    private UserSessionBean userSessionBean;

    @Inject
    private UserService userService;

    @Inject
    private EntityManager entityManager;


//    public List<FantasyTeamT> findTeamsAndLeaguesByUser(Long userId){
//        return entityManager.createQuery("select fantasyTeam from FantasyTeamT  fantasyTeam " +
//                " where fantasyTeam.userId = :id",FantasyTeamT.class)
//                .setParameter("id",userId)
//                .getResultList();
//    }

    public UserT uploadNewImageForUser(byte[] newImage,Long userId){
        UserT userT = entityManager.find(UserT.class,userId);
        userT.getUserInfo().setUserIcon(newImage);
        return userService.saveUser(userT);

    }


}

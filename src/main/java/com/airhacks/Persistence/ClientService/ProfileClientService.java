package com.airhacks.Persistence.ClientService;

import com.airhacks.Domain.FantasyLeagueD;
import com.airhacks.Domain.FantasyTeamD;
import com.airhacks.Resources.ApplicationResource;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Named("clientService")
@RequestScoped
public class ProfileClientService {


    WebTarget webTarget;


    @PostConstruct
    public void init() {
        webTarget = ApplicationResource.getApplicationClient().target("http://localhost:8080/FantasyLeagueService/resources/");
    }


    //find all fantasy teams of a user given his id
    public List<FantasyTeamD> findFantasyTeamsByUser(Long userId) {
        try {
             return  webTarget
                    .path("fantasyTeams/fantasyTeamsByUser/"+String.valueOf(userId))
                    .request(MediaType.APPLICATION_JSON)
                    .get(Response.class)
                     .readEntity(new GenericType<List<FantasyTeamD>>() {});
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public Response createFantasyLeague(FantasyLeagueD fantasyLeagueD,Long createdById) {
        //Invoke rest service to get user fantasy teams
        fantasyLeagueD.addLeagueOwner(createdById);
        fantasyLeagueD.addLeagueParticipant(createdById);
        return webTarget
                .path("fantasyLeagues/createLeague")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(fantasyLeagueD, MediaType.APPLICATION_JSON));
    }

    //localhost:8080/FantasyLeagueService/resources/fantasyUser/createFantasyUser
    public Response createFantasyUser(Long userId) {
        //Invoke rest service to create fantasy user after registration
        return webTarget
                .path("fantasyUser/createFantasyUser")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(userId, MediaType.APPLICATION_JSON));
    }

}

package com.airhacks.JPA;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "USER_ROLES")
public class UserRoleT implements Serializable {

    static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name ="USER_ROLE_ID")
    private Long userRoleId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="USER_NAME",referencedColumnName = "USER_NAME")
    private UserT user;

    @OneToOne
    @JoinColumn(name="ROLE_NAME",referencedColumnName = "ROLE_NAME")
    private RoleT role;

    public UserRoleT() {

    }


    public Long getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(Long userRoleId) {
        this.userRoleId = userRoleId;
    }

    public UserT getUser() {
        return user;
    }

    public void setUser(UserT user) {
        this.user = user;
    }

    public RoleT getRole() {
        return role;
    }

    public void setRole(RoleT role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserRoleT{" +
                "userRoleId=" + userRoleId +
                ", role='" + role + '\'' +
                '}';
    }
}

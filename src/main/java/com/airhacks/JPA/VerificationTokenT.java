package com.airhacks.JPA;


import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

@Entity
public class VerificationTokenT implements Serializable {
    private static final int EXPIRATION = 60 * 24;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String token;

    @OneToOne(targetEntity = UserT.class, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "USERS_ID")
    private UserT user;

    @Column
    private Date expirationDate;

    // standard constructors, getters and setters

    @PrePersist
    public void prePerist(){
        this.setExpirationDate(calculateExpiryDate(EXPIRATION));
    }

    public VerificationTokenT() {
    }

    private Date calculateExpiryDate(int expiryTimeInMinutes) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime()));
        cal.add(Calendar.MINUTE, expiryTimeInMinutes);
        return new Date(cal.getTime().getTime());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserT getUser() {
        return user;
    }

    public void setUser(UserT user) {
        this.user = user;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return "VerificationTokenT{" +
                "id=" + id +
                ", token='" + token + '\'' +
                ", user=" + user +
                ", expirationDate=" + expirationDate +
                '}';
    }
}
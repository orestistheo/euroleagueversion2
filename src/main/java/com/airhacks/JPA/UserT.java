package com.airhacks.JPA;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "USERS")
public class UserT implements Serializable {

    static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name ="USERS_ID")
    private Long userId;

    @NaturalId
    @Column(name ="USER_NAME")
    private String userName;

    @Column(name ="PASSWORD")
    private String password;

    @Column(name ="EMAIL")
    private String email;

    @OneToOne(fetch = FetchType.LAZY,mappedBy = "user",cascade = CascadeType.ALL)
    private UserRoleT userRoleT;

    @OneToOne(fetch = FetchType.LAZY,mappedBy = "user",cascade = CascadeType.ALL)
    private UserInfoT userInfo;

    @Column(name = "ENABLED")
    private boolean enabled;


    public UserT() {
        userRoleT=new UserRoleT();
        userInfo=new UserInfoT();
        this.enabled=false;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long toDoId) {
        this.userId = toDoId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserRoleT getUserRoleT() {
        return userRoleT;
    }

    public void setUserRoleT(UserRoleT userRoleT) {
        this.userRoleT = userRoleT;
    }

    public void addUserRole(UserRoleT userRoleT){
        this.userRoleT=userRoleT;
        userRoleT.setUser(this);
    }
    public void addUserInfo(UserInfoT userInfo){
        this.userInfo=userInfo;
        userInfo.setUser(this);
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public UserInfoT getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfoT userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public String toString() {
        return "UserT{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", userRoleT=" + userRoleT +
                '}';
    }
}

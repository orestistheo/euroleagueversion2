package com.airhacks.JPA;

import com.airhacks.Domain.enums.GenderEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "USER_INFO")
public class UserInfoT implements Serializable {

    static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name ="USER_INFO_ID")
    private Long userInfoId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="USER_ID")
    private UserT user;

    @Column(name ="USER_ICON")
    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] userIcon;

    @Column(name ="GENDER")
    @Enumerated(EnumType.STRING)
    private GenderEnum gender;

    @Column(name ="DATE_OF_BIRTH",nullable = false)
    private LocalDate dateOfBirth;

    @Column(name = "FAVOURITE_TEAM")
    private Long favouriteTeam;

    public UserInfoT() {
    }

    public Long getUserInfoId() {
        return userInfoId;
    }

    public void setUserInfoId(Long userInfoId) {
        this.userInfoId = userInfoId;
    }

    public UserT getUser() {
        return user;
    }

    public void setUser(UserT user) {
        this.user = user;
    }

    public byte[] getUserIcon() {
        return userIcon;
    }

    public void setUserIcon(byte[] userIcon) {
        this.userIcon = userIcon;
    }

    public GenderEnum getGender() {
        return gender;
    }

    public void setGender(GenderEnum gender) {
        this.gender = gender;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Long getFavouriteTeam() {
        return favouriteTeam;
    }

    public void setFavouriteTeam(Long favouriteTeam) {
        this.favouriteTeam = favouriteTeam;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "userInfoId=" + userInfoId +
                ", user=" + user +
                ", gender='" + gender + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                '}';
    }
}

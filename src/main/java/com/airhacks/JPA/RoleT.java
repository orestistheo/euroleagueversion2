package com.airhacks.JPA;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ROLES")
public class RoleT implements Serializable {

        static final long serialVersionUID = 1L;

        @GeneratedValue(strategy = GenerationType.AUTO)
        @Id
        @Column(name ="ROLES_ID")
        private Long userRoleId;

        @Column(name ="ROLE_NAME",nullable = false)
        private String roleName;

        @Column(name ="ROLE_DESCRIPTION")
        private String roleDescription;

        public RoleT() {

        }

    public Long getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(Long userRoleId) {
        this.userRoleId = userRoleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    @Override
    public String toString() {
        return "RoleT{" +
                "userRoleId=" + userRoleId +
                ", roleName='" + roleName + '\'' +
                ", roleDescription='" + roleDescription + '\'' +
                '}';
    }
}

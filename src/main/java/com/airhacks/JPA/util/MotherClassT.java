package com.airhacks.JPA.util;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class MotherClassT implements Serializable {

    static final long serialVersionUID = 1L;

    @Version
    private String version;


    private LocalDateTime creationDate;


    private LocalDateTime lastUpdatedDate;

    public MotherClassT() {
        creationDate = LocalDateTime.now();
    }

    @PrePersist
    private void prePersist(){
        if(creationDate==null)
            creationDate=LocalDateTime.now();

        lastUpdatedDate=LocalDateTime.now();
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(LocalDateTime lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    @Override
    public String toString() {
        return "MotherClassT{" +
                "version='" + version + '\'' +
                ", creationDate=" + creationDate +
                ", lastUpdatedDate=" + lastUpdatedDate +
                '}';
    }
}

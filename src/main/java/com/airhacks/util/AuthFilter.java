package com.airhacks.util;

import com.airhacks.Bean.UserSessionBean;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName="AuthFilter", urlPatterns={"/login.xhtml"})
public class AuthFilter implements Filter {


    @Inject
    private UserSessionBean userSessionBean;

    public AuthFilter() {
    }
    public void destroy() {
    }
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;

        boolean isLoggedIn = userSessionBean.getLoggedIn();
        // Check if the user is accessing login page
        if (req.getRequestURI().equals(req.getContextPath() + "/login.xhtml") && isLoggedIn) {
                // Redirect to landing or home page
                HttpServletResponse res = (HttpServletResponse) response;
                res.sendRedirect(req.getContextPath()
                        + "/userViews/welcomeUser.xhtml");
            } else {
                // Otherwise, nothing to do if he has not logged in
                // pass the request along the filter chain
                chain.doFilter(request, response);
            }
    }


    public void init(FilterConfig fConfig) throws ServletException {
    }
}

package com.airhacks.Domain;

import com.airhacks.Domain.enums.GenderEnum;

import java.io.Serializable;
import java.time.LocalDate;

public class UserInfoD implements Serializable {

    static final long serialVersionUID = 1L;

    private Long userInfoId;
    private UserD user;
    private byte[] userIcon;
    private GenderEnum gender;
    private LocalDate dateOfBirth;
    private String favouriteTeam;

    //helper fields
    private String genderString;


    public UserInfoD() {
    }

    public Long getUserInfoId() {
        return userInfoId;
    }

    public void setUserInfoId(Long userInfoId) {
        this.userInfoId = userInfoId;
    }

    public UserD getUser() {
        return user;
    }

    public void setUser(UserD user) {
        this.user = user;
    }

    public byte[] getUserIcon() {
        return userIcon;
    }

    public void setUserIcon(byte[] userIcon) {
        this.userIcon = userIcon;
    }

    public GenderEnum getGender() {
        return gender;
    }

    public void setGender(GenderEnum gender) {
        this.gender = gender;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getFavouriteTeam() {
        return favouriteTeam;
    }

    public void setFavouriteTeam(String favouriteTeam) {
        this.favouriteTeam = favouriteTeam;
    }

    public String getGenderString() {
        return genderString;
    }

    public void setGenderString(String genderString) {
        this.genderString = genderString;
    }

    @Override
    public String toString() {
        return "UserInfoD{" +
                "userInfoId=" + userInfoId +
                ", user=" + user +
                ", gender='" + gender + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", favouriteTeam='" + favouriteTeam + '\'' +
                '}';
    }
}

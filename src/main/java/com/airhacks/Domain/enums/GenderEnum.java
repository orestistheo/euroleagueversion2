package com.airhacks.Domain.enums;

import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.ArrayList;

public enum GenderEnum implements Serializable {

    MALE("1","ML","Male"),
    FEMALE("2","FML","Female"),
    OTHER("3","OTHR","Other");

    private String id;
    private String code;
    private String description;


    GenderEnum(String id, String code, String description) {
        this.id=id;
        this.code=code;
        this.description=description;
    }

    public static ArrayList<SelectItem> getEnumValues(){
        GenderEnum[] genderArray = values();
        ArrayList<SelectItem> list = new ArrayList<>();
        for(GenderEnum genderEnum:genderArray){
            list.add(new SelectItem(genderEnum.getId(),genderEnum.getDescription(),genderEnum.getDescription()));
        }
        return list;
    };


    public static GenderEnum getEnumById(String id){
        for(GenderEnum genderEnum: values()){
            if (genderEnum.id.equals(id))
            return genderEnum;
        }
            return null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

package com.airhacks.Domain.enums;

import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.ArrayList;

public enum FantasyLeagueTypeEnum implements Serializable {

    ROTO("1","ROTO","Roto"),
    H2H("2","H2H","Head To Head");

    private String id;
    private String code;
    private String description;


    FantasyLeagueTypeEnum(String id, String code, String description) {
        this.id=id;
        this.code=code;
        this.description=description;
    }

    public static ArrayList<SelectItem> getEnumValues(){
        FantasyLeagueTypeEnum[] genderArray = values();
        ArrayList<SelectItem> list = new ArrayList<>();
        for(FantasyLeagueTypeEnum fantasyLeagueTypeEnum:genderArray){
            list.add(new SelectItem(fantasyLeagueTypeEnum.getId(),fantasyLeagueTypeEnum.getDescription(),fantasyLeagueTypeEnum.getDescription()));
        }
        return list;
    };


    public static FantasyLeagueTypeEnum getEnumById(String id){
        for(FantasyLeagueTypeEnum fantasyLeagueTypeEnum: values()){
            if (fantasyLeagueTypeEnum.id.equals(id))
                return fantasyLeagueTypeEnum;
        }
        return null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

package com.airhacks.Domain;

import java.io.Serializable;

public class FantasyTeamD implements Serializable {

    static final long serialVersionUID = 1L;


    private Long fantasyTeamId;
    private FantasyLeagueD fantasyLeague;
    private byte[] teamIcon;
    private boolean active;
    private String teamName;
    private Integer teamRanking;
    private Long userId;

    public FantasyTeamD(Long fantasyTeamId,  boolean active, String teamName, Integer teamRanking, Long userId) {
        this.fantasyTeamId = fantasyTeamId;
        this.fantasyLeague = fantasyLeague;
        this.teamIcon = teamIcon;
        this.active = active;
        this.teamName = teamName;
        this.teamRanking = teamRanking;
        this.userId = userId;
    }

    public FantasyTeamD() {
    }

    public Long getFantasyTeamId() {
        return fantasyTeamId;
    }

    public void setFantasyTeamId(Long fantasyTeamId) {
        this.fantasyTeamId = fantasyTeamId;
    }

    public FantasyLeagueD getFantasyLeague() {
        return fantasyLeague;
    }

    public void setFantasyLeague(FantasyLeagueD fantasyLeague) {
        this.fantasyLeague = fantasyLeague;
    }

    public byte[] getTeamIcon() {
        return teamIcon;
    }

    public void setTeamIcon(byte[] teamIcon) {
        this.teamIcon = teamIcon;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Integer getTeamRanking() {
        return teamRanking;
    }

    public void setTeamRanking(Integer teamRanking) {
        this.teamRanking = teamRanking;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}

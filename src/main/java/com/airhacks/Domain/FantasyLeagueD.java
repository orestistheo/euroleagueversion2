package com.airhacks.Domain;

import javax.json.bind.annotation.JsonbTransient;
import java.io.Serializable;
import java.util.*;


public class FantasyLeagueD implements Serializable {

    static final long serialVersionUID = 1L;


    private Long fantasyLeagueId;
    private String leaguePassword;
    private String leagueType;
    private String leagueName;
    private Integer numberOfTeams;
    private byte[] leagueIcon;
    private Set leagueOwners;
    private Set leagueParticipants;

    @JsonbTransient
    private List<FantasyTeamD> fantasyTeams;

    @JsonbTransient
    //helper
    private String fantasyLeagueTypeString;

    public FantasyLeagueD() {
        fantasyTeams=new ArrayList<>();
        leagueOwners=new HashSet();
        leagueParticipants=new HashSet<>();
    }

    public FantasyLeagueD(Long fantasyLeagueId, String leagueCode, String leaguePassword, String leagueType, String leagueName, Integer numberOfTeams) {
        this.fantasyLeagueId = fantasyLeagueId;
        this.leaguePassword = leaguePassword;
        this.leagueType = leagueType;
        this.leagueName = leagueName;
        this.numberOfTeams = numberOfTeams;
    }

    public Long getFantasyLeagueId() {
        return fantasyLeagueId;
    }

    public void setFantasyLeagueId(Long fantasyLeagueId) {
        this.fantasyLeagueId = fantasyLeagueId;
    }

    public String getLeaguePassword() {
        return leaguePassword;
    }

    public void setLeaguePassword(String leaguePassword) {
        this.leaguePassword = leaguePassword;
    }

    public String getLeagueType() {
        return leagueType;
    }

    public void setLeagueType(String leagueType) {
        this.leagueType = leagueType;
    }

    public String getLeagueName() {
        return leagueName;
    }

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    public Integer getNumberOfTeams() {
        return numberOfTeams;
    }

    public void setNumberOfTeams(Integer numberOfTeams) {
        this.numberOfTeams = numberOfTeams;
    }

    public byte[] getLeagueIcon() {
        return leagueIcon;
    }

    public void setLeagueIcon(byte[] leagueIcon) {
        this.leagueIcon = leagueIcon;
    }

    public List<FantasyTeamD> getFantasyTeams() {
        return fantasyTeams;
    }

    public void setFantasyTeams(List<FantasyTeamD> fantasyTeams) {
        this.fantasyTeams = fantasyTeams;
    }

    public String getFantasyLeagueTypeString() {
        return fantasyLeagueTypeString;
    }

    public void setFantasyLeagueTypeString(String fantasyLeagueTypeString) {
        this.fantasyLeagueTypeString = fantasyLeagueTypeString;
    }

    public void addFantasyTeam(FantasyTeamD fantasyTeamD){
        this.fantasyTeams.add(fantasyTeamD);
        fantasyTeamD.setFantasyLeague(this);
    }

    public void addLeagueOwner(Long userId){
        this.leagueOwners.add(new FantasyUserD(userId));
    }

    public void addLeagueParticipant(Long userId){
        this.leagueParticipants.add(new FantasyUserD(userId));
    }

    public Set getLeagueOwners() {
        return leagueOwners;
    }

    public void setLeagueOwners(Set leagueOwners) {
        this.leagueOwners = leagueOwners;
    }

    public Set getLeagueParticipants() {
        return leagueParticipants;
    }

    public void setLeagueParticipants(Set leagueParticipants) {
        this.leagueParticipants = leagueParticipants;
    }

    @Override
    public String toString() {
        return "FantasyLeagueD{" +
                "fantasyLeagueId=" + fantasyLeagueId +
                ", leaguePassword='" + leaguePassword + '\'' +
                ", leagueType='" + leagueType + '\'' +
                ", leagueName='" + leagueName + '\'' +
                ", numberOfTeams=" + numberOfTeams +
                ", leagueIcon=" + Arrays.toString(leagueIcon) +
                '}';
    }
}

package com.airhacks.Domain;

import java.io.Serializable;

public class FantasyUserD implements Serializable {

    static final long serialVersionUID = 1L;


    private Long fantasyUserId;
    private Long userId;

    public FantasyUserD() {
    }

    public FantasyUserD(Long userId) {
        this.userId = userId;
    }

    public Long getFantasyUserId() {
        return fantasyUserId;
    }

    public void setFantasyUserId(Long fantasyUserId) {
        this.fantasyUserId = fantasyUserId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "FantasyUserD{" +
                "fantasyUserId=" + fantasyUserId +
                ", userId=" + userId +
                '}';
    }
}
